import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComponentProvaComponent } from './component-prova.component';

describe('ComponentProvaComponent', () => {
  let component: ComponentProvaComponent;
  let fixture: ComponentFixture<ComponentProvaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComponentProvaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComponentProvaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
