import { TestBed } from '@angular/core/testing';

import { ServeiProvaService } from './servei-prova.service';

describe('ServeiProvaService', () => {
  let service: ServeiProvaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServeiProvaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
